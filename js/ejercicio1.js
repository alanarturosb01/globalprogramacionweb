function calcularPulsaciones() {
    const edad = parseInt(document.getElementById('edadInput').value);
    let pulsacionesPorMinuto;

    if (edad >= 0 && edad <= 1) {
        pulsacionesPorMinuto = 150; 
    } else if (edad >= 2 && edad <= 12) {
        pulsacionesPorMinuto = 112.5; 
    } else if (edad >= 13 && edad <= 40) {
        pulsacionesPorMinuto = 75; 
    } else {
        pulsacionesPorMinuto = 65; 
    }

    
    const pulsacionesTotales = pulsacionesPorMinuto * 60 * 24 * 365.25;

    document.getElementById('resultado').textContent = `Tus pulsaciones totales son: ${pulsacionesTotales.toFixed(2)}`;
}