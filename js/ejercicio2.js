document.addEventListener("DOMContentLoaded", function() {
    const generarCalificacionesBtn = document.getElementById("generarCalificaciones");
    const calificacionesList = document.getElementById("calificaciones");
    const noAprobadosSpan = document.getElementById("noAprobados");
    const aprobadosSpan = document.getElementById("aprobados");
    const promedioNoAprobadosSpan = document.getElementById("promedioNoAprobados");
    const promedioAprobadosSpan = document.getElementById("promedioAprobados");
    const promedioGeneralSpan = document.getElementById("promedioGeneral");

    const calificaciones = [];

    function generarCalificaciones() {
        calificaciones.length = 0;
        calificacionesList.innerHTML = "";
        noAprobadosSpan.textContent = "";
        aprobadosSpan.textContent = "";
        promedioNoAprobadosSpan.textContent = "";
        promedioAprobadosSpan.textContent = "";
        promedioGeneralSpan.textContent = "";

        for (let i = 0; i < 35; i++) {
            const calificacion = Math.random() * 10;
            calificaciones.push(calificacion);
            const li = document.createElement("li");
            li.textContent = `Alumno ${i + 1}: ${calificacion.toFixed(2)}`;
            calificacionesList.appendChild(li);
        }

        const noAprobados = calificaciones.filter(calificacion => calificacion < 7);
        const aprobados = calificaciones.filter(calificacion => calificacion >= 7);
        const promedioNoAprobados = noAprobados.length > 0 ? noAprobados.reduce((a, b) => a + b) / noAprobados.length : 0;
        const promedioAprobados = aprobados.length > 0 ? aprobados.reduce((a, b) => a + b) / aprobados.length : 0;
        const promedioGeneral = calificaciones.length > 0 ? calificaciones.reduce((a, b) => a + b) / calificaciones.length : 0;

        noAprobadosSpan.textContent = noAprobados.length;
        aprobadosSpan.textContent = aprobados.length;
        promedioNoAprobadosSpan.textContent = promedioNoAprobados.toFixed(2);
        promedioAprobadosSpan.textContent = promedioAprobados.toFixed(2);
        promedioGeneralSpan.textContent = promedioGeneral.toFixed(2);
    }

    generarCalificacionesBtn.addEventListener("click", generarCalificaciones);
});
